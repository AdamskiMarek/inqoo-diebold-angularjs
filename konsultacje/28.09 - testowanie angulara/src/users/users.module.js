
angular.module('users', [])


angular.module('users').component('usersPage', {
  bindings: {},
  template:/* html */`<div>
    <h1>{{ $ctrl.title | uppercase }}</h1>
    <div class="row">
      <div class="col">
      
      <!-- <pre>{{ $ctrl.users | json:2 }}</pre> -->
      <users-list users="$ctrl.users" 
                  selected="$ctrl.selected"
                  on-select=" $ctrl.pageSelectUser( $event )"></users-list>  
      </div>
      <!--<pre>{{ $ctrl.selected | json:2 }}</pre>  -->
      <div class="col" ng-if="$ctrl.mode =='details'">
        <user-details user="$ctrl.selected" on-select=" $ctrl.mode = 'edit' "></user-details>  
      </div>
      <div class="col" ng-if="$ctrl.mode =='edit'">
        <user-editor user="$ctrl.selected" on-save="$ctrl.save($event)" ></user-editor>  
      </div>
    </div>
  </div>`,
  // controllerAs: '$ctrl', // == this
  controller: function (UsersService, $timeout) {
    this.title = 'Users Page'
    this.users = []
    this.mode = 'details'
    this.selected = null

    this.save = function(draft){
      this.selected = draft;
      this.mode = 'details'
      // UsersService.save(draft)
      // UsersService.fetchUsers().
    }

    this.pageSelectUser = function (user) {
      // console.log('pageSelectUser', user);
      this.selected = user
    }

    $timeout(() => {
      UsersService.fetchUsers().then(data => {
        this.users = data
        this.selected = this.users[0]
      })
    }, 2000)

  },
})



angular
  .module('users')
  .service('UsersService', UsersService)

function UsersService($q, $http) {
  this.fetchUsers = () => {
    return $q.resolve([
      { id: '123', name: 'Bardzo dluuuuga nazwao uzytkownika 123' },
      { id: '234', name: 'User 234' },
      { id: '345', name: 'User 345' },
    ])
  }
}


angular.module('users').component('usersList', {
  bindings: {
    users: '<',
    selected: '<',
    onSelect: '&'
  },
  template:/* html */`<div> 
  <h4>Users List</h4>
    <div class="list-group">
      <div class="list-group-item" 
      ng-repeat="user in $ctrl.users"
      ng-class="{active: $ctrl.selected.id === user.id}"
      ng-click="$ctrl.listSelectUser(user)">
        {{user.name}}
      </div>
    </div>
  </div>`,
  controller() {
    this.listSelectUser = function (user) {
      // console.log('listSelectUser', user);
      this.onSelect({ $event: user })
    }
  }
})

angular.module('users').component('userDetails', {
  bindings: {
    userData: '<user',
    onSelect: '&'
  },
  template:/* html */`<div> 
      <h4>User Details</h4>
      <dl><dt>Name:</dt><dd>{{ $ctrl.userData.name }}</dd></dl>
      <button ng-click=" $ctrl.onSelect() ">Edit</button>
  </div>`,
  // controllerAs: '$ctrl', // == this
})

angular.module('users').component('userEditor', {
  bindings: {
    userData: '<user',
    onSave: '&'
  },
  template:/* html */`<div>  
    <h4>Edit user</h4>
    <div class="form-group">
      <label>Name:</label>
      <input class="form-control" 
              ng-model="$ctrl.draft.name">
    </div>
    <button ng-click=" $ctrl.onSave({
      $event: $ctrl.draft
    }) ">Save</button>
    </div>`,
  // controllerAs: '$ctrl', // == this,
  controller() {
    this.$onChanges = function () {
      this.draft = { ...this.userData }
    }
  }
})