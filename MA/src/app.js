angular.module("config", []);

const app = angular.module("myApp", [
  "config",
  "tasks",
  "users",
  "usersFormCtrl",
  "settings",
  "ui.router",
]);

app.config(function ($stateProvider) {
  $stateProvider
    .state({ name: "tasks", url: "/tasks", component: "tasksPage" })
    .state({ name: "users", url: "/users", component: "usersPage" })
    .state({
      name: "settings",
      url: "/settings",
      templateUrl: "./vievs/settings-view.tpl.html",
    });
});

app.controller("AppCtrl", ($scope) => {
  $scope.title = "MyApp";
  $scope.user = { name: "Guest" };

  $scope.isLoggedIn = false;

  $scope.login = () => {
    $scope.user.name = "Admin";
    $scope.isLoggedIn = true;
  };

  $scope.logout = () => {
    $scope.user.name = "Guest";
    $scope.isLoggedIn = false;
  };

  $scope.pages = [
    { name: "tasks", label: "tasks", url: "./vievs/tasks-view.tpl.html" },
    { name: "users", label: "users", url: "./vievs/users-view.tpl.html" },
    {
      name: "settings",
      label: "settings",
      url: "./vievs/settings-view.tpl.html",
    },
  ];
  $scope.currentPage = $scope.pages[1];
  $scope.goToPage = (page) => {
    $scope.currentPage = $scope.pages.find((p) => p.name === page);
  };
});
