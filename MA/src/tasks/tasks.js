const tasks = angular.module("tasks", []);

angular
  .module("tasks")
  .component("tasksPage", {
    bindings: {},
    template: /* html */ `<div> TasksPage {{$ctrl.title}}
        <div class="row">
          <div class="col">
            <tasks-list tasks="$ctrl.tasks" 
                        on-select="$ctrl.select($event)"></tasks-list>
          </div>
          <div class="col">
            <task-details task="$ctrl.selected" on-cancel="$ctrl.cancel($event)" ng-if="$ctrl.selected" on-edit="$ctrl.edit($event)" >
            </task-details>
            <task-editor  task="$ctrl.selected"  on-cancel="$ctrl.cancel($event)" ng-if="$ctrl.selected && $ctrl.mode" on-save="$ctrl.save($event)"></task-editor>
          </div>
        </div>
      </div>`,
    controller($scope) {
      // const vm = $scope.$ctrl = {}
      this.title = "Tasks";
      this.mode = null;
      this.selected = null;
      this.tasks = [
        { id: 1, name: "task1", completed: true },
        { id: 2, name: "task2", completed: false },
        { id: 3, name: "task3", completed: true },
      ];
      this.select = (task) => {
        this.selected = task;
        this.mode = null;
      };
      this.cancel = (event) => {
        if (this.select !== null && this.mode !== null) {
          this.mode = null;
        } else this.selected = null;
      };
      this.edit = (event) => {
        this.mode = "edit";
      };
      this.save = (draft) => {
        this.tasks = this.tasks.map((task) =>
          task.id == draft.id ? draft : task
        );
      };
    },
  })
  .component("tasksList", {
    bindings: { tasks: "<", onSelect: "&" }, // on-select=" zyx = $event.completed "
    template: /* html */ `<div> List: <div class="list-group">
        <div class="list-group-item" ng-repeat="task in $ctrl.tasks" 
          ng-class="{active: task.id == $ctrl.selected.id}"
          ng-click="$ctrl.onSelect({$event: task}); $parent.$ctrl.selected = task;">
        {{$index+1}}. {{task.name}} </div>`,
  })
  .component("taskDetails", {
    transclude: true,
    bindings: { task: "=", onEdit: "&", onCancel: "&", onSelect: "&" },
    template: /* html */ `<div>tasksDetails <dl>
      <dt>Name</dt><dd>{{$ctrl.task.name}}</dd>
    </dl>
      <button ng-click="$ctrl.onEdit({$event:$ctrl.task})">Edit</button>
      <button ng-click="$ctrl.onCancel({$event:$ctrl.task})">Cancel</button>
    </div>`,
  })
  .component("taskEditor", {
    bindings: { task: "=", onEdit: "&", onCancel: "&", onSave: "&" },
    template: /* html */ `<div> taskForm:
        <div class="form-group mb-3">
          <label for="task_name">Name</label>
          <input id="task_name" type="text" class="form-control" 
                ng-model="$ctrl.draft.name">
        </div>

        <div class="form-group mb-3">
          <label for="task_completed">
            <input id="task_completed" ng-model="$ctrl.draft.completed" type="checkbox"> Completed</label>
        </div>

        <button ng-click="$ctrl.onSave({$event: $ctrl.draft})">Save</button>
        <button ng-click="$ctrl.onCancel({$event:$ctrl.task})">Cancel</button>
      </div>`,
    // controller:'TaskEditorCtrl as $ctrl'
    controller($scope) {
      this.$onInit = () => {
        this.draft = { ...this.task };
      };

      // https://docs.angularjs.org/guide/component#component-based-application-architecture
      this.$onChanges = (changes) => {
        console.log(changes);
      };
      this.$doCheck = () => {}; // after parent $digest
      this.$postLink = () => {}; // after all child DOM is ready

      this.$onDestroy = () => {
        console.log("bye bye!");
      };
    },
    // controllerAs: '$ctrl'
  });

tasks.constant("INITIAL_TASKS", []);
tasks.constant("INITIAL_MODE", "show");

/* 
https://docs.angularjs.org/api/auto/service/$injector

// inferred (only works if code not minified/obfuscated)
$injector.invoke(function(serviceA){});

// annotated
function explicit(serviceA) {};
explicit.$inject = ['serviceA'];
$injector.invoke(explicit);

// inline
$injector.invoke(['serviceA', function(serviceA){}]);
*/

tasks.controller("TasksCtrl", TasksCtrl);

TasksCtrl.$inject = ["$scope", "INITIAL_TASKS", "INITIAL_MODE"];
function TasksCtrl($scope, INITIAL_TASKS, INITIAL_MODE) {
  console.log("Hello TasksCtrl", { ...$scope });

  $scope.mode = INITIAL_MODE;
  $scope.tasks = INITIAL_TASKS;
  $scope.filtered = [];

  $scope.draft = {};

  $scope.selectedTask = {
    id: "123",
    name: "Task 123",
    completed: true,
  };

  $scope.query = "";
  $scope.changeQuery = () => {
    $scope.filtered = $scope.tasks.filter((t) =>
      t.name.toLocaleLowerCase().includes($scope.query.toLocaleLowerCase())
    );
  };

  $scope.counts = { completed: 0, total: 0 };
  $scope.updateCounts = () => {
    $scope.counts = $scope.tasks.reduce(
      (counts, task) => {
        counts.total += 1;
        if (task.completed) {
          counts.completed += 1;
        }
        return counts;
      },
      { completed: 0, total: 0 }
    );
  };

  // shallow compares immutable values ( === )
  $scope.$watch("tasks", (newVal, oldVal) => {
    // $scope.$watchCollection('tasks', (newVal, oldVal) => {
    // debugger
    console.log(" task watcher update");
    $scope.changeQuery("");
    $scope.updateCounts();
  });
  // $scope.changeQuery('')

  $scope.edit = () => {
    $scope.mode = "edit";
    // $scope.draft = $scope.selectedTask
    $scope.draft = { ...$scope.selectedTask };
  };

  $scope.cancel = () => {
    $scope.mode = "show";
  };

  $scope.toggleCompleted = (draft) => {
    draft.completed = !draft.completed;
    $scope.tasks = $scope.tasks.map((task) =>
      task.id == draft.id ? draft : task
    );
  };

  $scope.save = () => {
    $scope.mode = "show";
    $scope.selectedTask = { ...$scope.draft };
    $scope.tasks = $scope.tasks.map((task) =>
      task.id == $scope.draft.id ? $scope.draft : task
    );
    // $scope.changeQuery($scope.query)
  };

  $scope.addTask = () => {
    // $scope.tasks.push({ // mutable wont trigger $watch!
    $scope.tasks = [
      ...$scope.tasks,
      {
        // Immutable copy // can use simple $watch
        id: Date.now().toString(),
        name: $scope.fastTaskName,
        completed: false,
      },
    ];
    $scope.fastTaskName = "";
    // $scope.changeQuery($scope.query)
  };

  $scope.remove = (id) => {
    $scope.tasks = $scope.tasks.filter((task) => task.id !== id);
    // $scope.changeQuery($scope.query)
  };

  $scope.select = (item) => {
    $scope.selectedTask = item;
  };
}
