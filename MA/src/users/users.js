const users = angular.module("users", ["users.service"]);

angular
  .module("users")
  .component("usersPage", {
    bindings: {},
    template: /* html */ `<div
    class="container"
  >
    <div class="row">
      <div class="col-5">
        <user-lists users="$ctrl.users" tasks="$ctrl.tasks" ></user-lists>
      </div>
      <div class="col">
        <user-details ></user-details>
        <user-editor ></user-editor>
      </div>
    </div>
  </div>`,
    controller($scope, UsersService) {
      this.users = []
      UsersService.getUsers().then(data =>{
        this.users = data
      });


      this.tasks = [
        { id: 1, name: "task1", completed: true },
        { id: 2, name: "task2", completed: false },
        { id: 3, name: "task3", completed: true },
      ];
      console.log(this.users);
    },
  })
  .component("userLists", {
    bindings: {users:"=", tasks:"="},
    template: /* html */ `<h4>Users List</h4>
    <div
    class="list-group"
    ng-repeat="user in $ctrl.users"
    >
    <span>{{$index+1}}.{{user.username}}</span>
    </div>`,
  })
  .component("userDetails", {
    bindings: {},
    template: /* html */ `<h4>Selected user details</h4>
    <dl>
      <dt>Nazwa</dt>
      <dd>{{usersPage.selectUser.username}}</dd>
    </dl>
    <button class="btn btn-info" ng-click="edit = true">Edit</button>`,
  })
  .component("userEditor", {
    bindings: {},
    template: /* html */ `<form
    ng-controller="usersFormCtrl"
    name="usersPage.form"
    novalidate
  >
    <div class="form-group mb-3">
      <label for="name">Name</label>
      <input
        id="name"
        type="text"
        name="name"
        class="form-control"
        ng-model="usersPage.selectUser.username"
        required
        minlength="3"
      />
      <div
        ng-if="usersPage.form['name'].$dirty ||
          usersPage.form['name'].$touched ||
          usersPage.form['name'].$submitted"
      >
        <p
          class="text-danger"
          ng-if="usersPage.form['name'].$error.required"
        >
          Name is required
        </p>
        <p
          class="text-danger"
          ng-if="usersPage.form['name'].$error.minlength"
        >
          too short! need more than 3
        </p>
      </div>
    </div>
    <!-- <pre>{{usersPage.form | json:2}}</pre> -->
    <button class="btn btn-danger" ng-click="edit = false">Cancel</button>
    <button class="btn btn-info" ng-click="usersPage.save()">save</button>
  </form>`,
  });

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

angular.module("users").controller("UsersPageCtrl", ($scope, UsersService) => {
  // console.log('Hello UsersPageCtrl', UsersService)
  const vm = ($scope.usersPage = {});

  vm.selected = null;
  vm.users = UsersService.getUsers();
  vm.selectUser = {};

  // vm.select = (id) => {
  //   vm.selected = UsersService.getUserById(id);
  //   vm.selectUser =  vm.selected
  // };

  vm.select = (id) => {
    UsersService.getUserById(id).then((data) => {
      vm.selected = data;
      vm.selectUser = vm.selected;
    });
  };

  vm.refresh = () => {
    UsersService.fetchUsers().then((data) => {
      vm.users = data;
    });
  };
  vm.refresh();

  vm.save = (draft) => {
    if (vm.form.$invalid) {
      alert("niepoprawny formularz");
      return;
    }
    vm.selected = UsersService.updateUser(draft);
    vm.select(draft.id);
    vm.users = UsersService.getUsers();
  };
});
