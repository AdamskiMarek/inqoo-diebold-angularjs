angular.module("users.service", []).service("UsersService", function ($http) {
  console.log("Hello UsersService");

  this._users = [
    { id: "123", username: "Admin" },
    { id: "234", username: "Gość" },
    { id: "546", username: "Karol" },
  ];

  this.fetchUsers = () => {
    return $http.get("http://localhost:3000/users").then((res) => {
      return (this._users = res.data);
    });
  };

  this.getUsers = () => {
    return $http.get("http://localhost:3000/users").then((res) => {
      return res.data;
    });
  };

  this.updateUser = (draft) => {
    return $http
      .put(`http://localhost:3000/users/${user.id}`, user)
      .then((resp) => resp.data);
  };

  this.createUser = (draft) => {
    return $http.put("http://localhost:3000/users", draft).then((res) => {
      return res.data;
    });
  };

  this.deleteUser = (draft) => {
    return $http
      .delete("http://localhost:3000/users/" + draft.id)
      .then((res) => {
        return res.data;
      });
  };

  this.getUserById = (id) => {
    return $http.get("http://localhost:3000/users/" + id).then((res) => {
      return res.data;
    });
  };
});
