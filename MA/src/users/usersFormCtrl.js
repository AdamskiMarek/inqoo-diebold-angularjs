angular.module("usersFormCtrl", []).controller("usersFormCtrl", ($scope) => {
  const vm = ($scope.usersForm = {});

  vm.editUser = {};

  vm.save = (selectUser) => {
    selectUser.name = vm.editUser.name;
  };
});
