
const app = angular.module('app', ['InvoiceEditor'])

const invoiceEditor = angular.module('InvoiceEditor', [])
invoiceEditor.controller('InvoiceEditorCtrl', ($scope) => {

  const invoice = {
    positions: [
      {
        id: "123",
        title: 'Pozycja 123',
        netto: 1000,
        tax: 23,
        brutto: 1230
      },
      {
        id: "234",
        title: 'Pozycja 234',
        netto: 2000,
        tax: 23,
        brutto: 2460
      },
    ],
    summary: {
      total: 2460
    }
  }
  $scope.invoice = invoice

  $scope.invoicePosition = {}

  // $scope.addPosition = () => {
  //   invoice.positions.push({});
  // }
 
  $scope.addPosition = (index) => {
    invoice.positions.splice(index+1,0,{brutto: 0});
    $scope.summary();
  }

  $scope.removePosition = (index) => { 
   invoice.positions.splice(index,1);
   $scope.summary();
  }

  $scope.summary = () => {
    invoice.summary.total = invoice.positions.reduce(
      (sum, position) => {
        return sum + position.brutto
      }, 0
    );
  }

  $scope.summary()
})
