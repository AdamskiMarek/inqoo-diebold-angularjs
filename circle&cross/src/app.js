const app = angular.module("myApp", ["circle&Cross"]);

app.controller("AppCtrl", ($scope) => {
  $scope.title = "MyApp";
  $scope.user = { name: "Guest" };

  $scope.isLoggedIn = false;

  $scope.login = () => {
    $scope.user.name = "Admin";
    $scope.isLoggedIn = true;
  };

  $scope.logout = () => {
    $scope.user.name = "Guest";
    $scope.isLoggedIn = false;
  };

  $scope.pages = [
    {
      name: "users",
      label: "users",
      url: "./views/users-view.tpl.html",
    },
    {
      name: "circle&cross",
      label: "circle&cross",
      url: "./views/circle&cross-view.tpl.html",
    },
  ];
  $scope.currentPage = $scope.pages[1];
  $scope.goToPage = (page) => {
    $scope.currentPage = $scope.pages.find((p) => p.name === page);
  };
});
