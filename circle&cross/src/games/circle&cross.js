const circleAndCross = angular.module("circle&Cross", []);

circleAndCross.controller("CircleAndCrossPageCtrl", ($scope) => {
  console.log("Hello circle&crossPageCtrl");
  const vm = ($scope.circleAndCrossPage = {});

  vm.fields = [];
  vm.winTables = ["123", "456", "789", "147", "258", "369", "159", "357"];

  vm.createFields = () => {
    for (let i = 0; i < 9; i++) {
      let field = { id: i, sign: "" };
      vm.fields.push(field);
    }
  };
  vm.createFields();

  vm.userMove = (index) => {
    vm.fields[index].sign = "o";
    vm.isWinner("o");
    vm.computerMove();
  };

  vm.computerMove = () => {
    let numberChoice;
    do {
      numberChoice = Math.floor(Math.random() * 9);
      if (vm.fields[numberChoice].sign !== "") {
        numberChoice = -1;
      }
    } while (numberChoice < 0);
    vm.fields[numberChoice].sign = "x";
    vm.isWinner("x");
  };

  vm.isWinner = (sign) => {
    let takenFields = { id: "" };
    for (let i = 0; i < vm.fields.length; i++) {
      if (vm.fields[i].sign == sign) {
        takenFields.id += String(i + 1);
      }
    }
    console.log(String(takenFields.id));
    console.log(vm.winTables.indexOf(takenFields.id));
    if (vm.winTables.indexOf(takenFields.id) !== -1) {
      console.log("you won");
      return
    }
  };
});
